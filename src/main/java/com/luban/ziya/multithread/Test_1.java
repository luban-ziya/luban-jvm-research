package com.luban.ziya.multithread;

import java.util.concurrent.locks.ReentrantLock;

/**
 * Created By ziya
 * QQ: 3039277701
 * 2021/10/17
 */
public class Test_1 {

    static ReentrantLock lock = new ReentrantLock(true);
    static int v = 0;

    public static void main(String[] args) throws InterruptedException {
        Thread t1 = new Thread(() -> {
            add();
            add2();
        });
        Thread t2 = new Thread(() -> add());

        t1.start();
        t2.start();

        t1.join();
        t2.join();

        System.out.println(v);
    }

    public static void add() {
        lock.lock();

        for (int i = 0; i < 10000; i++) {
            v++;
        }

        lock.unlock();
    }

    public static void add2() {
        lock.lock();

        for (int i = 0; i < 10000; i++) {
            v++;
        }

        lock.unlock();
    }
}

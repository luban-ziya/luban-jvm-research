package com.luban.ziya.classload;

/**
 * Created By ziya
 * 2020/8/2
 */
public class InitDeadLock {
    public static void main(String[] args) throws InterruptedException {
        new Thread(() -> new A()).start();
        new Thread(() -> new B()).start();
    }
}

class A {
    static {
        System.out.println("class A init");
        new B();
    }
}

class B {
    static {
        System.out.println("class B init");
        new A();
    }
}